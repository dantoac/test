#!/usr/bin/env python3
#unicode: utf-8

def get_costo(operacion):
    """retorna el costo según operación.
    Inicialmente está solamente definido el costo para 
    abrir y cerrar."""

    costo = { 
        'abrir': 20, 
        'cerrar': 35
        }


    # Si la operación es desconocida, retorna costo 0
    # esto podría ser engañoso, dependiendo del problema,
    # podría ser mejor retornar -1, evaluar False o también
    # elevar alguna excepción para ser capturada luego.
    return costo.get(operacion, 0)

def openRing(a):
    """Abre el anillo `a` para unirlo con el anterior"""
    print (f'Abriendo {a}')
    return get_costo('abrir')
    

def joinRings(a, b):
    """Une el anillo `a` abierto con uno `b` actualmente cerrado"""
    print (f'Uniendo {a} y {b}')
    return get_costo('cerrar')
    
def build_chain(X):
    """Retorna el costo de construir una cadena con
    una cantidad de X anillos.
    """
        
    total = 0

    for i in range(X-1):

        a = i + 1 #anillo siguiente
        b = i #anillo actual
        
        #suma el costo de cada operación al total
        total += openRing(a)
        total += joinRings(a, b)
        

    print (f"La opción más barata costó {total}")

    return total


build_chain(5)
